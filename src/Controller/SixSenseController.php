<?php

namespace Drupal\acquia_6sense\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sixsense\SixSenseServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class SixSenseController extends ControllerBase {

  /**
   * 6sense API service.
   *
   * @var mixed|string
   */
  protected $sixSenseService;

  public function __construct(SixSenseServiceInterface $sixSenseService) {
    $this->sixSenseService = $sixSenseService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sixsense')
    );
  }

  public function getData() {
    $data = $this->sixSenseService->getCompanyDetails();
    return new JsonResponse($data);
  }

}
