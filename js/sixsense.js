(function (Drupal) {

  'use strict';

  Drupal.behaviors.sixsense = {
    attach: function () {
      // Check if data was already fetched and stored in localStorage.
      // If not, make an AJAX request to get the data.
      if (!localStorage.getItem('sixsense')) {
        const response = fetch('/sixsense/company-identification')
          .then(response => response.json())
          .then(data => {
            localStorage.setItem('sixsense', JSON.stringify(data));
            console.log('6sense data fetched and stored.');
          });
      }
      else {
        console.log('6sense data is already in localStorage.');
      }
    }
  }

})(Drupal);
