(function (Drupal, drupalSettings) {

  // Get 6sense info.
  const data = JSON.parse(localStorage.getItem('sixsense'));

  // Initialize Acquia Personalization.
  window.AcquiaLift = {
    /*
    ** ACCOUNT PROPERTIES | MANDATORY | Should be the same across all sites in the same account
    ** Update account_id value below to match what's displayed in the sidebar of all your sites in the "Sites" section of Profile Manager
    */
    //account_id: "demo",
    liftAssetsURL: drupalSettings.acquia6Sense.liftAssetsURL,
    liftDecisionAPIURL: "https://us.perz-api.cloudservices.acquia.io",
    contentReplacementMode: "trusted",
    apiVersion: "v3",

    /*
    ** SITE ID | MANDATORY | Should be different for each site and environment in the same account
    ** Update site_id to match the value found in the sidebar of your site in the "Sites" section of Profile Manager
    */
    site_id: drupalSettings.acquia6Sense.liftAssetsURL,

    /** PAGE-SPECIFIC PROPERTIES | OPTIONAL | Content values can be different from page to page **/
    profile: {
      /* CUSTOM COLUMN META DATA | OPTIONAL | Content values can be different from page to page
      ** The itemprops need to be created from Lift Profile Manager
      ** Admin > Manage Configuration Data > Custom Column Meta Data.
      ** Will always be the format below of {table}_udf{accessor_number} -->
      **/
      'person_udf4':data.company.domain,
      'person_udf5':data.company.name,
      'person_udf6':data.company.region,
      'person_udf7':data.company.country,
      'person_udf8':data.company.state,
      'person_udf9':data.company.city,
      'person_udf10':data.company.industry,
    }
  };
})(Drupal, drupalSettings);
